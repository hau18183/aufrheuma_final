﻿using System.Collections.Generic;

namespace T7_AR_V2
{
    public static class Kontakte
    {
        public static List<Kontakt> KontaktList = new List<Kontakt>
        {
            new Kontakt() {Vorname="Jonas", Name = "Ortner",Beschreibung="col", ArztID=2},
            new Kontakt() {Vorname="Felix", Name = "Kiessling",Beschreibung="suber", ArztID=2},
             new Kontakt() {Vorname="Arthur", Name = "Hauser",Beschreibung="nice", ArztID=2},

        };
    }
}

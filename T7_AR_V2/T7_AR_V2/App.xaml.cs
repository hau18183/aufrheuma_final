﻿using System.IO;
using Xamarin.Forms;
using Android;
using System;

namespace T7_AR_V2
{
    public partial class App : Application
    {
        public static TabbedPage tabbedPage;
        public App()
        {

           /* HubConnectionBuilder hubConnection = new HubConnectionBuilder()
        .WithUrl("{https://yoururlhere.com or ip:port or localhost:port" + "/chatHub")
        .Build();*/

            RW_Berichte.berichte_path.Clear();
            RW_Berichte.berichte.Clear();
            RunningApp.setLogin(false);
            RunningApp.setNachname(null,null);
            RunningApp.setVorname(null, null);
            //File config = Android.Net.Uri.Parse("file:///android_asset/AR_CONFIG.txt");
            var config = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "AR_CONFIG.txt");
            //File.WriteAllText(config, String.Empty);
            if (config == null || !File.Exists(config))
            {
                File.Create(config);
            }
            else
            {
                Splitter s = new Splitter();
                s.readFromSafe();
            }


            //if(ra.getNachname()==null || ra.getVorname() == null)

            InitializeComponent();
            tabbedPage = new TabbedPage();
            
            tabbedPage.SelectedTabColor = Xamarin.Forms.Color.Teal;
            tabbedPage.UnselectedTabColor = Xamarin.Forms.Color.DarkTurquoise;
            tabbedPage.BarBackgroundColor = Xamarin.Forms.Color.PaleTurquoise;
            tabbedPage.Children.Add(new Page1_Login());
            if (!RunningApp.getIsArzt() && RunningApp.getLogin())
            {
                
                tabbedPage.Children.Add(new Page2_Studie());
                tabbedPage.Children.Add(new Page3_Bericht());
                tabbedPage.Children.Add(new Page4_Chat());
            }
            else if(!RunningApp.getIsArzt() && !RunningApp.getLogin())
            {
                  //do nothing
            }
            else if(RunningApp.getIsArzt() && !RunningApp.getLogin())
            {
                //do nothing
            }
            else if(RunningApp.getIsArzt() && RunningApp.getLogin())
            {
                //do something
                tabbedPage.Children.Add(new Page1_Arzt_CreateCodes());
                tabbedPage.Children.Add(new Page2_Arzt_Kontakte());
            }
            
            
            

            MainPage = tabbedPage;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            Splitter s = new Splitter();
            if (RunningApp.getLogin())
            {
                var config = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "AR_CONFIG.txt");
                File.WriteAllText(config, String.Empty);
                s.writeInSafe();
            }
            //schreibe in AR_CONFIG
            // Handle when your app sleeps
            /* streamWriter.WriteLine($"{AR_Decoder.Encode(RunningApp.getVorname())}");
                     streamWriter.WriteLine($"{AR_Decoder.Encode(RunningApp.getNachname())}");*/
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}

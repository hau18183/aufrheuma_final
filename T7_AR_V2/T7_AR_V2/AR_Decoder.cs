﻿using System;

namespace T7_AR_V2
{
    static class AR_Decoder
    {
        public static String Decode(string usercode)
        {
            char[] code;
            code = usercode.ToCharArray();
            int count = 0;
            foreach (char x in code)
            {
                switch (x)
                {
                    case '1': code[count] = 'a'; break;

                    case '2': code[count] = 'b'; break;

                    case '3': code[count] = 'c'; break;

                    case '4': code[count] = 'd'; break;

                    case '5': code[count] = 'e'; break;

                    case '6': code[count] = 'f'; break;

                    case '7': code[count] = 'g'; break;

                    case '8': code[count] = 'h'; break;

                    case '9': code[count] = 'i'; break;

                    case '0': code[count] = 'j'; break;

                    case 'a': code[count] = 'k'; break;

                    case 'b': code[count] = 'l'; break;

                    case 'c': code[count] = 'm'; break;

                    case 'd': code[count] = 'n'; break;

                    case 'e': code[count] = 'o'; break;

                    case 'f': code[count] = 'p'; break;

                    case 'g': code[count] = 'q'; break;

                    case 'h': code[count] = 'r'; break;

                    case 'i': code[count] = 's'; break;

                    case 'j': code[count] = 't'; break;

                    case 'k': code[count] = 'u'; break;

                    case 'l': code[count] = 'v'; break;

                    case 'm': code[count] = 'w'; break;

                    case 'n': code[count] = 'x'; break;

                    case 'o': code[count] = 'y'; break;

                    case 'p': code[count] = 'z'; break;

                    default: break;
                }
                count++;
            }
            return new String(code);
        }

        public static String Encode(string usercode)
        {
            char[] code;
            code = usercode.ToCharArray();
            int count = 0;
            foreach (char x in code)
            {
                switch (x)
                {
                    case 'a': code[count] = '1'; break;

                    case 'b': code[count] = '2'; break;

                    case 'c': code[count] = '3'; break;

                    case 'd': code[count] = '4'; break;

                    case 'e': code[count] = '5'; break;

                    case 'f': code[count] = '6'; break;

                    case 'g': code[count] = '7'; break;

                    case 'h': code[count] = '8'; break;

                    case 'i': code[count] = '9'; break;

                    case 'j': code[count] = '0'; break;

                    case 'k': code[count] = 'a'; break;

                    case 'l': code[count] = 'b'; break;

                    case 'm': code[count] = 'c'; break;

                    case 'n': code[count] = 'd'; break;

                    case 'o': code[count] = 'e'; break;

                    case 'p': code[count] = 'f'; break;

                    case 'q': code[count] = 'g'; break;

                    case 'r': code[count] = 'h'; break;

                    case 's': code[count] = 'i'; break;

                    case 't': code[count] = 'j'; break;

                    case 'u': code[count] = 'k'; break;

                    case 'v': code[count] = 'l'; break;

                    case 'w': code[count] = 'm'; break;

                    case 'x': code[count] = 'n'; break;

                    case 'y': code[count] = 'o'; break;

                    case 'z': code[count] = 'p'; break;

                    default: break;
                }
                count++;
            }
            return new String(code);
        }


    }
}







/*String str = "Geeks, For Geeks";

String[] spearator = { "s, ", "For" };
Int32 count = 2;

// using the method 
String[] strlist = str.Split(spearator, count,
       StringSplitOptions.RemoveEmptyEntries); 
  
        foreach(String s in strlist) 
        { 
            Console.WriteLine(s); 
        } 
    } 
} 
Output:

Geek
 Geeks*/

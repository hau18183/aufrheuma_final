﻿namespace T7_AR_V2
{
    public class Kontakt
    {
        public string Vorname { get; set; }
        public string Name { get; set; }
        public string Beschreibung { get; set; }
        public int ArztID { get; set; } //set false for de jure vegetables, like pizza
    }
}


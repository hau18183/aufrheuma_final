﻿namespace T7_AR_V2
{
    public class DataSource
    {
        public string Name { set; get; }
        public string Date { set; get; }

        public DataSource(string name, string date)
        {
            Name = name;
            Date = date;

        }
    }
}

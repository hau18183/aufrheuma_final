﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace T7_AR_V2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1_Login : ContentPage
    {
        public Page1_Login()
        {
            
            InitializeComponent();
            if (RunningApp.getLogin() == true)
            {
                Willkommen.Text = "Willkommen zurück!";
                Login.IsVisible = false;
                ArztID.IsVisible = false;
                LoginDaten.IsVisible = true;
                LoginDaten2.IsVisible = true;
                LoginDaten.Text = RunningApp.getVorname() + " " + RunningApp.getNachname();


            }
            else
            {
                Willkommen.Text = "Login / Registration";
                Login.IsVisible = true;
                ArztID.IsVisible = true;
                LoginDaten.IsVisible = false;
                LoginDaten2.IsVisible = false;
            }

            /*DisplayAlert("Alert", "You clicked on tail", "OK");*/
        }

        private void Login_Clicked(object sender, EventArgs e)
        {
            Console.WriteLine(AR_Decoder.Encode("admin"));

            if (ArztID.Text !=null)
            {
                Splitter s = new Splitter();
                if (s.split(ArztID.Text.ToString().ToLower(), 3))
                {
                    if (RunningApp.getIsArzt())
                    {
                        App.tabbedPage.Children.Add(new Page1_Arzt_CreateCodes());
                        App.tabbedPage.Children.Add(new Page2_Arzt_Kontakte());
                    }
                    else
                    {
                        App.tabbedPage.Children.Add(new Page2_Studie());
                        App.tabbedPage.Children.Add(new Page3_Bericht());
                        App.tabbedPage.Children.Add(new Page4_Chat());
                    }
                    RunningApp.setLogin(true);
                    Willkommen.Text = "Willkommen zurück!";
                    Login.IsVisible = false;
                    ArztID.IsVisible = false;
                    LoginDaten.IsVisible = true;
                    LoginDaten.Text = RunningApp.getVorname() + " " + RunningApp.getNachname();
                }
            }


        }

        /* streamWriter.WriteLine($"{AR_Decoder.Encode(RunningApp.getVorname())}");
                     streamWriter.WriteLine($"{AR_Decoder.Encode(RunningApp.getNachname())}");*/

        /*async void LoginClicked(object sender, EventArgs args)
        {
            DatabaseConnection bc;
            await bc = new DatabaseConnection();
        }*/
    }
}
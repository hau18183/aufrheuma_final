﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace T7_AR_V2
{
    public class Message
    {
        public string text{ get; set; }
        public DateTime date { get; set; }
        public bool incoming { get; set; }

        public TextAlignment TextAlign { get; set; }
        public LayoutOptions BoxAlign { get; set; }

        public Message(string x, DateTime y, bool z, TextAlignment a,  LayoutOptions b)
        {
            text = x;
            date = y;
            incoming = z;
            TextAlign = a;
            BoxAlign = b;
        }
    }
}

﻿using System;

namespace T7_AR_V2
{
    static class RunningApp
    {
        private static readonly String AdminKey = "admin"; //1fi083646a420i01l2nnoj379
        private static String Vorname = null;
        private static String Nachname = null;
        private static bool LoggedIn = false;
        private static bool isArzt = false;
        public static String temp_key { set; get; }
        public static void setVorname(string x, string adminkey)
        {
            if(x==null && adminkey == null) { Vorname = null; }
            //jonas-hauser-14c9d
            if (adminkey != null && adminkey.Length > 0 && checkCode(adminkey)==true)
            {
                if (Vorname == null && x != null && x.Length > 0)
                    Vorname = x;
                else if (Vorname != null && x != null && checkCode(x))
                {
                    Vorname = null;
                }
            }
        }

        public static void setIsArzt(bool x) { isArzt = x; }
        public static bool getIsArzt() { return isArzt; }

        public static void setNachname(string x, string adminkey)
        {
            if (x == null && adminkey == null) { Nachname = null; }
            if (adminkey != null && adminkey.Length > 0 && checkCode(adminkey)==true)
            {
                if (Nachname == null && x != null && x.Length > 0)
                    Nachname = x;
                else if (Nachname != null && x != null && checkCode(x))
                {
                    Nachname = null;
                }
            }
        }

        public static void setLogin(bool x)
        {
            LoggedIn = x;
        }

        public static String getVorname() { return Vorname; }

        public static String getNachname() { return Nachname; }

        public static bool getLogin() { return LoggedIn; }



        public static bool checkCode(string adminkey) {
            string translated_key;
            translated_key = AR_Decoder.Decode(adminkey).ToLower();
            if (AdminKey.ToLower().Equals(translated_key))
            {
                return true;
            }
            else { return false; } 
        }
    }
}

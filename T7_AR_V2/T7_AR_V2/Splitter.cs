﻿using System;
using System.Collections.Generic;
using System.IO;
using Xamarin.Essentials;

namespace T7_AR_V2
{
    class Splitter
    {

        public bool split(string usercode, int count)
        {
            string[] strList = null;
            string[] seperator = { "-" };
            strList = usercode.Split(seperator, count, StringSplitOptions.RemoveEmptyEntries);
            if (strList != null && strList.Length >= 2)
            {
                if (RunningApp.checkCode(strList[2]))
                {
                    RunningApp.temp_key = strList[2];
                    RunningApp.setVorname(strList[0], strList[2]);
                    RunningApp.setNachname(strList[1], strList[2]);
                    return true;
                }
                return false;
            }

            return false;
        }
        public void writeInSafe()
        {
            int count =0;
            var config = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "AR_CONFIG.txt");
            File.WriteAllText(config, String.Empty);
            using (var streamWriter = new StreamWriter(config))
                {
                if (RW_Berichte.berichte_path == null || RW_Berichte.berichte_path.Count <= 0)
                {
                    streamWriter.WriteLine(count);
                }
                else
                {
                    string path_to_write = "- ";
                    foreach (string path in RW_Berichte.berichte_path)
                    {
                        path_to_write += path + " - ";
                        count++;
                    }
                    path_to_write += count.ToString();
                    streamWriter.WriteLine(path_to_write);
                }
                    count = 0;
                if (RW_Berichte.berichte == null || RW_Berichte.berichte.Count <= 0)
                {
                    streamWriter.WriteLine(count);
                }
                else
                {
                    string datasource_to_write="- ";
                    foreach (DataSource path in RW_Berichte.berichte)
                    {
                        datasource_to_write += path.Name + " ; " + path.Date.ToString() + " - ";
                        count++;
                    }
                    datasource_to_write += count;
                    streamWriter.WriteLine(datasource_to_write);
                }
                    streamWriter.WriteLine($"true");
                    streamWriter.WriteLine($"{RunningApp.getVorname()}");
                    streamWriter.WriteLine($"{RunningApp.getNachname()}");
                    streamWriter.WriteLine($"{RunningApp.temp_key}");
                    //login!
                    streamWriter.Close();
                
                

            }
            /*string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string filename = Path.Combine(documentsPath, "AR_CONFIG");

            using (var streamWriter = new StreamWriter(filename, true))
            {
                streamWriter.Write("-");
                int count = 0;
                foreach (string path in RW_Berichte.berichte_path)
                {
                    streamWriter.Write($"{path}");
                    streamWriter.Write("-");
                    count++;
                }
                streamWriter.Write(count);
                count = 0;
                streamWriter.WriteLine("-");
                foreach (DataSource path in RW_Berichte.berichte)
                {
                    streamWriter.Write($"{path.Name}");
                    streamWriter.Write(";");
                    streamWriter.Write($"{path.Date}");
                    streamWriter.Write("-");
                    count++;
                }
                streamWriter.Write(count);
                streamWriter.WriteLine($"true");
                streamWriter.WriteLine($"{RunningApp.getVorname()}");
                streamWriter.WriteLine($"{RunningApp.getNachname()}");
                streamWriter.WriteLine($"{RunningApp.temp_key}");
                //login!
                streamWriter.Close();
            }*/
        }

        public void readFromSafe()
        {
            var config = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "AR_CONFIG.txt");

                using (var streamReader = new StreamReader(config))
            {
                string[] paths = getPaths(streamReader.ReadLine());
                if (paths != null) { 
                foreach (string x in paths)
                {
                        if (x != null && x.Length > 0 && x != "" && x != " ")
                            RW_Berichte.berichte_path.Add(x);
                        else
                            break;
                }
            }
                List<DataSource> ds = getDataSources(streamReader.ReadLine());
                if (ds != null) {
                    foreach (DataSource x in ds)
                    {
                        if (x != null)
                        {
                            RW_Berichte.berichte.Add(x);
                        }
                        else { break; }
                    }
                }
                if (ds != null && paths != null) {
                    RunningApp.setLogin(bool.Parse(streamReader.ReadLine()));
                    string admin, vorname, nachname;
                    vorname = streamReader.ReadLine();
                    nachname = streamReader.ReadLine();
                    admin = streamReader.ReadLine();
                    if (RunningApp.checkCode(admin))
                    {
                        RunningApp.setVorname(vorname, admin);
                        RunningApp.setNachname(nachname, admin);
                    }
                    streamReader.Close();
                }
                streamReader.Close();
                
            }
            /*string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string filename = Path.Combine(documentsPath, "AR_CONFIG");

            using (var streamReader = new StreamReader(filename, true))
            {
                foreach(string x in getPaths(streamReader.ReadLine()))
                {
                    RW_Berichte.berichte_path.Add(x);
                }
                foreach (DataSource x in getDataSources(streamReader.ReadLine()))
                {
                    RW_Berichte.berichte.Add(x);
                }
                RunningApp.setLogin(bool.Parse(streamReader.ReadLine()));
                string admin, vorname, nachname;
                vorname = streamReader.ReadLine();
                nachname = streamReader.ReadLine();
                admin = streamReader.ReadLine();
                if (RunningApp.checkCode(admin))
                {
                    RunningApp.setVorname(vorname, admin);
                    RunningApp.setNachname(nachname, admin);
                }
                streamReader.Close();
            }*/
        }

        public string[] getPaths(string x)
        {
            if (x != null && x.Length > 0 && x != "" && x != " ")
            {
                int count = 0;
                //string check = x[x.Length - 1].ToString();
                count = int.Parse(x[x.Length - 1].ToString()) +1;
                x = x.Substring(0, x.Length - 1);
                string[] strList = null;
                string[] seperator = { "-", " -", " - ", "- " };
                strList = x.Split(seperator, count, StringSplitOptions.RemoveEmptyEntries);
                return strList;
            }
            return null;
        }

        public List<DataSource> getDataSources(string x)
        {
            if (x != null && x.Length > 0 && x != "" && x != " ")
            {
                List < DataSource > ds = new List<DataSource>();
                int count = 0;
                count = int.Parse(x[x.Length - 1].ToString())+1;
                x = x.Substring(0, x.Length - 1);
                string[] strList;
                string[] strListItems;
                string[] seperator = { "-", " -", " - ", "- " };
                strList = x.Split(seperator, count, StringSplitOptions.RemoveEmptyEntries);
                seperator[0]=";";
                for (int y=0; y<strList.Length-1; y++)
                {   
                    strListItems = strList[y].Split(seperator, 2, StringSplitOptions.RemoveEmptyEntries);
                    ds.Add(new DataSource(strListItems[0], strListItems[1]));               
                }
                return ds;
            }
            return null;
        }
            
    }
}
/*public static string[] Decode(string usercodes, int count)
       {
           string[,] strList = new string[count, 2];
           String[] seperator = { "-" };
           int y;
           for (int x = 0; x < count; x++)
           { 
               for(y = 0; y<2; y++)
               {
                   //Code
                   strList[x] =


                   //Code
                   if (y == 1) y = 0;
               }
           }
           strList = usercodes.Split(seperator, count, StringSplitOptions.RemoveEmptyEntries);

           return strList;
       }*/

﻿
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace T7_AR_V2
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page4_Chat : ContentPage
    {
        
        private ObservableCollection<Message> _messages;
        public ObservableCollection<Message> Messages
        {
            get
            {
                return _messages ?? (_messages = RunningMessages.running_messages);
            }
        }
        public Page4_Chat()
        {
            InitializeComponent();
        }

        private void Senden_Clicked(object sender, System.EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                if (Nachricht.Text != "" && Nachricht.Text != null)
                {
                    Message msg = new Message(Nachricht.Text, DateTime.Now, false, TextAlignment.End, LayoutOptions.End);
                    RunningMessages.running_messages.Add(msg);
                    _messages = RunningMessages.running_messages;
                    Nachricht.Text = String.Empty;
                    MSGS.ItemsSource = Messages;
                }
            }

        }
    }
}
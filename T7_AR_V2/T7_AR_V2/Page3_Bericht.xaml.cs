﻿
using System;
using System.Collections.ObjectModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace T7_AR_V2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3_Bericht : ContentPage
    {
        private ObservableCollection<DataSource> _berichte;
        public ObservableCollection<DataSource> Berichte
        {
            get
            {
                return _berichte ?? (_berichte = RW_Berichte.berichte);
            }
        }
        public Page3_Bericht()
        {
            InitializeComponent();

        }

        protected override void OnAppearing()
        {

            base.OnAppearing();
            _berichte = RW_Berichte.berichte;
            Berichts.ItemsSource = Berichte;
            if (RW_Berichte.berichte.Count == 0)
            {
                Text1.IsVisible = true;
                Text2.IsVisible = true;
                Text3.IsVisible = true;
            }
            else
            {
                Text1.IsVisible = false;
                Text2.IsVisible = false;
                Text3.IsVisible = false;
            }

        }

        private void ImageButton_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                //Console.WriteLine(Berichts.SelectedItem.ToString());
            }
        }


    }

}
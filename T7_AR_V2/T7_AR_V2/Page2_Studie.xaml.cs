﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace T7_AR_V2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2_Studie : ContentPage
    {
        private int[] positions;
        private int position;
        private List<string> bodyparts_clicked;
        private List<string> berichte_path;

        private List<Object> body = new List<Object>();
        private List<Object> r_arm = new List<Object>();
        private List<Object> l_arm = new List<Object>();
        private List<Object> r_leg = new List<Object>();
        private List<Object> l_leg = new List<Object>();

        //private string Schmerzart;
        public Page2_Studie()
        {
            positions = new int[5];
            positions[0] = -2;
            positions[1] = -1;
            positions[2] = 0;
            positions[3] = 1;
            positions[4] = 2;
            position = 2;
            bodyparts_clicked = new List<string>();
            berichte_path = new List<string>();


            InitializeComponent();
            addAll();
            Tag.Text = "Körper";
            activate(body);
            deactivate(r_arm);
            deactivate(r_leg);
            deactivate(l_arm);
            deactivate(l_leg);
            berichte_path = RW_Berichte.berichte_path;
            Date_Bericht.Text = DateTime.Now.ToString("dd.MM.yyyy", DateTimeFormatInfo.InvariantInfo);
            Nr_Bericht.Text = (berichte_path.Count + 1).ToString();
            /*var boxTapHandler = new TapGestureRecognizer();
            boxTapHandler.Tapped += ChangeColor;
            r_ankle.GestureRecognizers.Add(boxTapHandler);*/
        }

        private void Head_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    bodyparts_clicked.Add("Head");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    bodyparts_clicked.Remove("Head");
                }
            }

        }

        private void R_Shoulder_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    bodyparts_clicked.Add("R_Shoulder");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    bodyparts_clicked.Remove("R_Shoulder");
                }
            }
        }

        private void L_Shoulder_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    bodyparts_clicked.Add("L_Shoulder");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    bodyparts_clicked.Remove("L_Shoulder");
                }
            }
        }

        private void R_Wrist_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    bodyparts_clicked.Add("R_Wrist");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    bodyparts_clicked.Remove("R_Wrist");
                }
            }
        }

        private void L_Wrist_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {

                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    bodyparts_clicked.Add("L_Wrist");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    bodyparts_clicked.Remove("L_Wrist");
                }
            }
        }

        private void R_Ellbow_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    bodyparts_clicked.Add("R_Ellbow");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    bodyparts_clicked.Remove("R_Ellbow");
                }
            }
        }

        private void L_Ellbow_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    bodyparts_clicked.Add("L_Ellbow");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    bodyparts_clicked.Remove("L_Ellbow");
                }
            }
        }

        private void R_Hip_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    bodyparts_clicked.Add("R_Hip");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    bodyparts_clicked.Remove("R_Hip");
                }
            }
        }

        private void L_Hip_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    bodyparts_clicked.Add("L_Hip");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    bodyparts_clicked.Remove("L_Hip");
                }
            }
        }

        private void R_Knee_Clicked(object sender, EventArgs e)
        {

            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    bodyparts_clicked.Add("R_Knee");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    bodyparts_clicked.Remove("R_Knee");
                }
            }

        }

        private void L_Knee_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    bodyparts_clicked.Add("L_Knee");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    bodyparts_clicked.Remove("L_Knee");
                }
            }
        }

        private void R_Ankle_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    bodyparts_clicked.Add("R_Ankle");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    bodyparts_clicked.Remove("R_Ankle");
                }
            }
        }

        private void L_Ankle_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    bodyparts_clicked.Add("L_Ankle");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    bodyparts_clicked.Remove("L_Ankle");
                }
            }


        }

        private void Speichern_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                int i = berichte_path.Count +1;
                string filename_temp = "Bericht_" + i.ToString();
                bool doesExist = File.Exists(filename_temp);
                if (!doesExist)
                {
                    var filename = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), filename_temp);
                    //File.WriteAllText(config, String.Empty);
                    if (filename == null)
                    {
                        File.Create(filename);
                    }
                    using (var streamWriter = new StreamWriter(filename, true))
                    {
                        string parts = "-";
                        foreach (string bodypart in bodyparts_clicked)
                        {
                            parts += bodypart + "-";
                        }
                        parts += bodyparts_clicked.Count.ToString();
                        streamWriter.WriteLine(parts);
                        switch ((bool)Schmerzart.IsChecked)
                        {
                            case true: streamWriter.WriteLine("Druckschmerzhafte Gelenke"); break;

                            case false: streamWriter.WriteLine("Geschwollene Gelenke"); break;

                            default: break;
                        }
                        streamWriter.WriteLine(infos.Text);

                        //login!
                        streamWriter.Close();
                    }

                    RW_Berichte.berichte.Add(new DataSource(filename_temp, DateTime.Now.ToString("dd.MM.yyyy", DateTimeFormatInfo.InvariantInfo).ToString()));
                    RW_Berichte.berichte_path.Add(filename);
                    berichte_path = RW_Berichte.berichte_path;
                    Nr_Bericht.Text = (berichte_path.Count +1).ToString();
                    
                }
            }
        }

        private void addAll()
        {
            body.Add(Head);
            body.Add(R_Shoulder);
            body.Add(R_Ellbow);
            body.Add(R_Wrist);
            body.Add(R_Hip);
            body.Add(R_Knee);
            body.Add(R_Ankle);
            body.Add(L_Shoulder);
            body.Add(L_Ellbow);
            body.Add(L_Wrist);
            body.Add(L_Hip);
            body.Add(L_Knee);
            body.Add(L_Ankle);
            body.Add(body1);
            body.Add(body2);
            body.Add(body3);
            body.Add(body4);
            body.Add(body5);
            body.Add(body6);

            r_arm.Add(arm1);
            r_arm.Add(r_thumb);
            r_arm.Add(arm3);
            r_arm.Add(arm4);
            r_arm.Add(arm5);
            r_arm.Add(arm6);
            r_arm.Add(r_thumb_1);
            r_arm.Add(r_thumb_2);
            r_arm.Add(point_1);
            r_arm.Add(point_2);
            r_arm.Add(point_3);
            r_arm.Add(middle_1);
            r_arm.Add(middle_2);
            r_arm.Add(middle_3);
            r_arm.Add(ring_1);
            r_arm.Add(ring_2);
            r_arm.Add(ring_3);
            r_arm.Add(small_1);
            r_arm.Add(small_2);
            r_arm.Add(small_3);

            l_arm.Add(arm1);
            l_arm.Add(l_thumb);
            l_arm.Add(arm3);
            l_arm.Add(arm4);
           l_arm.Add(arm5);
            l_arm.Add(arm6);
            l_arm.Add(l_thumb_1);
            l_arm.Add(l_thumb_2);
            l_arm.Add(point_1);
            l_arm.Add(point_2);
            l_arm.Add(point_3);
            l_arm.Add(middle_1);
            l_arm.Add(middle_2);
            l_arm.Add(middle_3);
            l_arm.Add(ring_1);
            l_arm.Add(ring_2);
            l_arm.Add(ring_3);
            l_arm.Add(small_1);
            l_arm.Add(small_2);
            l_arm.Add(small_3);

            r_leg.Add(arm1);
            r_leg.Add(l_thumb);
            r_leg.Add(arm3);
            r_leg.Add(arm4);
            r_leg.Add(arm5);
            r_leg.Add(arm6);
            r_leg.Add(l_thumb_1);
            r_leg.Add(l_thumb_2);
            r_leg.Add(point_1);
            r_leg.Add(point_2);
            r_leg.Add(point_3);
            r_leg.Add(middle_1);
            r_leg.Add(middle_2);
            r_leg.Add(middle_3);
            r_leg.Add(ring_1);
            r_leg.Add(ring_2);
            r_leg.Add(ring_3);
            r_leg.Add(small_1);
            r_leg.Add(small_2);
            r_leg.Add(small_3);
            r_leg.Add(r_ferse);

            l_leg.Add(arm1);
            l_leg.Add(r_thumb);
            l_leg.Add(arm3);
            l_leg.Add(arm4);
            l_leg.Add(arm5);
            l_leg.Add(arm6);
            l_leg.Add(r_thumb_1);
            l_leg.Add(r_thumb_2);
            l_leg.Add(point_1);
            l_leg.Add(point_2);
            l_leg.Add(point_3);
            l_leg.Add(middle_1);
            l_leg.Add(middle_2);
            l_leg.Add(middle_3);
            l_leg.Add(ring_1);
            l_leg.Add(ring_2);
            l_leg.Add(ring_3);
            l_leg.Add(small_1);
            l_leg.Add(small_2);
            l_leg.Add(small_3);
            l_leg.Add(l_ferse);




        }

        private void URL_Clicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.dipha.at/?etheme_portfolio=rheuma-studie"));
        }

        private void Left_Clicked(object sender, EventArgs e)
        {
            position += 1;
            if (position == 5)
            {
                position -= 1;
            }
            else
            {
                switch (positions[position])
                {
                    case -2: activate(r_leg); deactivate(r_arm); Tag.Text = "Rechter Fuß"; break;
                    case -1: deactivate(r_leg);activate(r_arm); deactivate(body); Tag.Text = "Rechte Hand"; break;
                    case 0: deactivate(r_arm); activate(body); deactivate(l_arm); Tag.Text = "Körper"; break;
                    case 1: deactivate(body); deactivate(r_leg); activate(l_arm); Tag.Text = "Linke Hand"; break;
                    case 2: deactivate(l_arm);activate(l_leg); Tag.Text = "Linker Fuß"; break;
                    default: break;
                }
            }

        }

        private void deactivate(List<Object> x)
        {
            foreach(Object y in x)
            {
                if(y is Button)
                {
                    var test = y as Button;
                    test.IsVisible = false;
                    test.IsEnabled = false;
                }
                else if(y is BoxView)
                {
                    var test = y as BoxView;
                    test.IsVisible = false;
                    test.IsEnabled = false;
                }
            }
        }

        private void activate(List<Object> x)
        {
            foreach (Object y in x)
            {
                if (y is Button)
                {
                    var test = y as Button;
                    test.IsVisible = true;
                    test.IsEnabled = true;
                }
                else if (y is BoxView)
                {
                    var test = y as BoxView;
                    test.IsVisible = true;
                    test.IsEnabled = true;
                }
            }
        }

        private void Right_Clicked(object sender, EventArgs e)
        {
            position -= 1;
            if (position == -1)
            {
                position += 1;
            }
            else
            {
                switch (positions[position])
                {
                    case -2: deactivate(r_arm); activate(r_leg); Tag.Text = "Rechter Fuß"; break;
                    case -1: deactivate(r_leg); deactivate(body); activate(r_arm); Tag.Text = "Reche Hand"; break;
                    case 0: deactivate(r_arm); deactivate(l_arm); activate(body); Tag.Text = "Körper"; break;
                    case 1: deactivate(l_leg); deactivate(body); activate(l_arm); Tag.Text = "Linke Hand"; break;
                    case 2: deactivate(l_arm); activate(l_leg); Tag.Text = "Linker Fuß"; break;
                    default: break;
                }
            }
        }

        private void r_thumb_1_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 1)
                        bodyparts_clicked.Add("R_Thumb_1");
                    else if (position == 4)
                        bodyparts_clicked.Add("L_BigToe_1");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 1)
                        bodyparts_clicked.Remove("R_Thumb_1");
                    else if (position == 4)
                        bodyparts_clicked.Remove("L_BigToe_1");
                }
            }
        }

        private void point_1_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 0)
                        bodyparts_clicked.Add("R_PointToe_1");
                    else if (position == 1)
                        bodyparts_clicked.Add("R_Pointfinger_1");
                    else if (position == 3)
                        bodyparts_clicked.Add("L_Pointfinger_1");
                    else if (position == 4)
                        bodyparts_clicked.Add("L_PointToe_1");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 0)
                        bodyparts_clicked.Remove("R_PointToe_1");
                    else if (position == 1)
                        bodyparts_clicked.Remove("R_Pointfinger_1");
                    else if (position == 3)
                        bodyparts_clicked.Remove("L_Pointfinger_1");
                    else if (position == 4)
                        bodyparts_clicked.Remove("L_PointToe_1");
                }
            }
        }

        private void middle_1_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 0)
                        bodyparts_clicked.Add("R_MiddleToe_1");
                    else if (position == 1)
                        bodyparts_clicked.Add("R_Middlefinger_1");
                    else if (position == 3)
                        bodyparts_clicked.Add("L_Middlefinger_1");
                    else if (position == 4)
                        bodyparts_clicked.Add("L_MiddleToe_1");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 0)
                        bodyparts_clicked.Remove("R_MiddleToe_1");
                    else if (position == 1)
                        bodyparts_clicked.Remove("R_Middlefinger_1");
                    else if (position == 3)
                        bodyparts_clicked.Remove("L_Middlefinger_1");
                    else if (position == 4)
                        bodyparts_clicked.Remove("L_MiddleToe_1");
                }
            }
        }

        private void ring_1_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 0)
                        bodyparts_clicked.Add("R_RingToe_1");
                    else if (position == 1)
                        bodyparts_clicked.Add("R_Ringfinger_1");
                    else if (position == 3)
                        bodyparts_clicked.Add("L_Ringfinger_1");
                    else if (position == 4)
                        bodyparts_clicked.Add("L_RingToe_1");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 0)
                        bodyparts_clicked.Remove("R_RingToe_1");
                    else if (position == 1)
                        bodyparts_clicked.Remove("R_Ringfinger_1");
                    else if (position == 3)
                        bodyparts_clicked.Remove("L_Ringfinger_1");
                    else if (position == 4)
                        bodyparts_clicked.Remove("L_RingToe_1");
                }
            }
        }



        private void point_2_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 0)
                        bodyparts_clicked.Add("R_PointToe_2");
                    else if (position == 1)
                        bodyparts_clicked.Add("R_Pointfinger_2");
                    else if (position == 3)
                        bodyparts_clicked.Add("L_Pointfinger_2");
                    else if (position == 4)
                        bodyparts_clicked.Add("L_PointToe_2");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 0)
                        bodyparts_clicked.Remove("R_PointToe_2");
                    else if (position == 1)
                        bodyparts_clicked.Remove("R_Pointfinger_2");
                    else if (position == 3)
                        bodyparts_clicked.Remove("L_Pointfinger_2");
                    else if (position == 4)
                        bodyparts_clicked.Remove("L_PointToe_2");
                }
            }
        }

        private void middle_2_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 0)
                        bodyparts_clicked.Add("R_MiddleToe_2");
                    else if (position == 1)
                        bodyparts_clicked.Add("R_Middlefinger_2");
                    else if (position == 3)
                        bodyparts_clicked.Add("L_Middlefinger_2");
                    else if (position == 4)
                        bodyparts_clicked.Add("L_MiddleToe_2");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 0)
                        bodyparts_clicked.Remove("R_MiddleToe_2");
                    else if (position == 1)
                        bodyparts_clicked.Remove("R_Middlefinger_2");
                    else if (position == 3)
                        bodyparts_clicked.Remove("L_Middlefinger_2");
                    else if (position == 4)
                        bodyparts_clicked.Remove("L_MiddleToe_2");
                }
            }
        }

        private void ring_2_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 0)
                        bodyparts_clicked.Add("R_RingToe_2");
                    else if (position == 1)
                        bodyparts_clicked.Add("R_Ringfinger_2");
                    else if (position == 3)
                        bodyparts_clicked.Add("L_Ringfinger_2");
                    else if (position == 4)
                        bodyparts_clicked.Add("L_RingToe_2");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 0)
                        bodyparts_clicked.Remove("R_RingToe_2");
                    else if (position == 1)
                        bodyparts_clicked.Remove("R_Ringfinger_2");
                    else if (position == 3)
                        bodyparts_clicked.Remove("L_Ringfinger_2");
                    else if (position == 4)
                        bodyparts_clicked.Remove("L_RingToe_2");
                }
            }
        }

        private void point_3_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 0)
                        bodyparts_clicked.Add("R_PointToe_3");
                    else if (position == 1)
                        bodyparts_clicked.Add("R_Pointfinger_3");
                    else if (position == 3)
                        bodyparts_clicked.Add("L_Pointfinger_3");
                    else if (position == 4)
                        bodyparts_clicked.Add("L_PointToe_3");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 0)
                        bodyparts_clicked.Remove("R_PointToe_3");
                    else if (position == 1)
                        bodyparts_clicked.Remove("R_Pointfinger_3");
                    else if (position == 3)
                        bodyparts_clicked.Remove("L_Pointfinger_3");
                    else if (position == 4)
                        bodyparts_clicked.Remove("L_PointToe_3");
                }
            }
        }

        private void middle_3_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 0)
                        bodyparts_clicked.Add("R_MiddleToe_3");
                    else if (position == 1)
                        bodyparts_clicked.Add("R_Middlefinger_3");
                    else if (position == 3)
                        bodyparts_clicked.Add("L_Middlefinger_3");
                    else if (position == 4)
                        bodyparts_clicked.Add("L_MiddleToe_3");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 0)
                        bodyparts_clicked.Remove("R_MiddleToe_3");
                    else if (position == 1)
                        bodyparts_clicked.Remove("R_Middlefinger_3");
                    else if (position == 3)
                        bodyparts_clicked.Remove("L_Middlefinger_3");
                    else if (position == 4)
                        bodyparts_clicked.Remove("L_MiddleToe_3");
                }
            }
        }

        private void ring_3_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 0)
                        bodyparts_clicked.Add("R_RingToe_3");
                    else if (position == 1)
                        bodyparts_clicked.Add("R_Ringfinger_3");
                    else if (position == 3)
                        bodyparts_clicked.Add("L_Ringfinger_3");
                    else if (position == 4)
                        bodyparts_clicked.Add("L_RingToe_3");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 0)
                        bodyparts_clicked.Remove("R_RingToe_3");
                    else if (position == 1)
                        bodyparts_clicked.Remove("R_Ringfinger_3");
                    else if (position == 3)
                        bodyparts_clicked.Remove("L_Ringfinger_3");
                    else if (position == 4)
                        bodyparts_clicked.Remove("L_RingToe_3");
                }
            }
        }

        private void small_2_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 0)
                        bodyparts_clicked.Add("R_SmallToe_2");
                    else if (position == 1)
                        bodyparts_clicked.Add("R_Pinkie_2");
                    else if (position == 3)
                        bodyparts_clicked.Add("L_Pinkie_2");
                    else if (position == 4)
                        bodyparts_clicked.Add("L_SmallToe_2");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 0)
                        bodyparts_clicked.Remove("R_SmallToe_2");
                    else if (position == 1)
                        bodyparts_clicked.Remove("R_Pinkie_2");
                    else if (position == 3)
                        bodyparts_clicked.Remove("L_Pinkie_2");
                    else if (position == 4)
                        bodyparts_clicked.Remove("L_SmallToe_2");
                }
            }
        }
        private void small_1_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 0)
                        bodyparts_clicked.Add("R_SmallToe_1");
                    else if (position == 1)
                        bodyparts_clicked.Add("R_Pinkie_1");
                    else if (position == 3)
                        bodyparts_clicked.Add("L_Pinkie_1");
                    else if (position == 4)
                        bodyparts_clicked.Add("L_SmallToe_1");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 0)
                        bodyparts_clicked.Remove("R_SmallToe_1");
                    else if (position == 1)
                        bodyparts_clicked.Remove("R_Pinkie_1");
                    else if (position == 3)
                        bodyparts_clicked.Remove("L_Pinkie_1");
                    else if (position == 4)
                        bodyparts_clicked.Remove("L_SmallToe_1");
                }
            }
        }

        private void small_3_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 0)
                        bodyparts_clicked.Add("R_SmallToe_3");
                    else if (position == 1)
                        bodyparts_clicked.Add("R_Pinkie_3");
                    else if (position == 3)
                        bodyparts_clicked.Add("L_Pinkie_3");
                    else if (position == 4)
                        bodyparts_clicked.Add("L_SmallToe_3");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 0)
                        bodyparts_clicked.Remove("R_SmallToe_3");
                    else if (position == 1)
                        bodyparts_clicked.Remove("R_Pinkie_3");
                    else if (position == 3)
                        bodyparts_clicked.Remove("L_Pinkie_3");
                    else if (position == 4)
                        bodyparts_clicked.Remove("L_SmallToe_3");
                }
            }

        }

        private void r_thumb_2_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 1)
                        bodyparts_clicked.Add("R_Thumb_2");
                    else if (position == 4)
                        bodyparts_clicked.Add("L_BigToe_2");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 1)
                        bodyparts_clicked.Remove("R_Thumb_2");
                    else if (position == 4)
                        bodyparts_clicked.Remove("L_BigToe_2");
                }
            }

        }

        private void l_thumb_1_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 3)
                        bodyparts_clicked.Add("L_Thumb_1");
                    else if (position == 0)
                        bodyparts_clicked.Add("R_BigToe_1");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 3)
                        bodyparts_clicked.Remove("L_Thumb_1");
                    else if (position == 0)
                        bodyparts_clicked.Remove("R_BigToe_1");
                }
            }

        }

        private void l_thumb_2_Clicked(object sender, EventArgs e)
        {
            if (RunningApp.getLogin())
            {
                Button btn = (Button)sender;
                if (btn.BorderColor == Color.ForestGreen)
                {
                    btn.BorderColor = Color.DarkRed;
                    if (position == 3)
                        bodyparts_clicked.Add("L_Thumb_2");
                    else if (position == 0)
                        bodyparts_clicked.Add("R_BigToe_2");
                }
                else
                {
                    btn.BorderColor = Color.ForestGreen;
                    if (position == 3)
                        bodyparts_clicked.Remove("L_Thumb_2");
                    else if (position == 0)
                        bodyparts_clicked.Remove("R_BigToe_2");
                }
            }

        }


        /*private void ChangeColor(object sender, EventArgs args)
        {
            BoxView bv = new BoxView();
            bv.Color = Color.Green;
            bv.Rotation = r_ankle.Rotation;
            bv.AnchorX = r_ankle.AnchorX;
            bv.AnchorY = r_ankle.AnchorY;
        }*/

        /*void OnTapGestureRecognizerTapped(object sender, EventArgs args)
        {

            var boxViewSender = (BoxView)sender;
            if(boxViewSender.Color == Color.Green)
            {
                boxViewSender.Color = Color.Red;
            }
            else
            {
                boxViewSender.Color = Color.Green;
            }
        }*/
    }
}